/*
*   Nomor 1. Ubah ke arrow function
*/

const golden = ()=>{
  console.log("this is golden!!")
}
 
golden();

/*
*   Nomor 2. Ubah ke object literal
*/

const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName(){
        console.log(this.firstName + " " + this.lastName)
        return 
      }
    }
  }
   
newFunction("William", "Imoh").fullName(); 

/*
*   Nomor 3. object destructuring
*/

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };

  const {firstName,lastName,destination,occupation,spell} = newObject;
  console.log(firstName, lastName, destination, occupation);

/*
*   Nomor 4. array spreading
*/

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east];

console.log(combined);

/*
*   Nomor 5. template literal
*/

const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
// Driver Code 
console.log(before) 