<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        .done{
            text-decoration: line-through;
        }
    </style>
</head>
<body>
    <div id="app">
        <input type="text" v-model="newTodo" @keyup.enter="addTodo">
        <ul>
            <li v-for="(todo, index) in todos">
                <span :class="{'done': todo.done}">@{{todo.text}}</span>
                <button v-on:click="removeTodo(index)">X</button>
                <button v-on:click="doneTodo(index)">DONE</button>
            </li>
        </ul>
    </div>

    <script src="{{asset('/js/vue-script.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <script>
        let vm = new Vue({
            el:"#app",
            data:{
                newTodo:"",
                todos: []
            },
            methods:{
                addTodo: function(){
                    let todo = this.newTodo.trim()
                    this.$http.post('/api/todo',{text:todo,done:0}).then(response => {
                        this.todos.unshift({
                            id : response.body.data.id,
                            text: todo,
                            done: response.body.data.done
                        })
                        this.newTodo='';
                    });
                },
                removeTodo: function(index){
                    this.$http.delete('/api/todo/'+this.todos[index].id).then(response => { 
                        this.todos.splice(index,1);
                    });
                },
                doneTodo: function(index){
                    todo = this.todos[index];
                    this.$http.put('/api/todo/'+ todo.id).then(response => {      
                        this.todos[index].done = !this.todos[index].done;
                    });
                }
            },
            mounted: function(){
                // GET /someUrl
                this.$http.get('/api/todo').then(response => {
                    this.todos = response.body.data
                });
            }
        });
    </script>
</body>
</html>