<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->server->all()["REQUEST_URI"];
        $role = Auth::user()->role;

        //Jika super admin, diloloskan
        if ($role == 2){                            
            return $next($request);
        }
        //Jika admin :
        else if($role == 1){       

            //loloskan jika mengakses route admin (/route-2)
            if (strpos($route,"/route-1") === false) {      
                return $next($request);                     
            }
            //redirect jika mengakses route superadmin
            else{                                           
                return redirect('/route-2');                
            }
        }
        // redirect ke route-3 jika merupakan guest
        return redirect('/route-3');
    }
}
