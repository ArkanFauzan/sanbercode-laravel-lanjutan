<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use Illuminate\Http\Request;
use App\Models\BorrowBook;
use App\Http\Resources\BorrowResource;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrow_books = BorrowBook::all();

        return BorrowResource::collection($borrow_books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_peminjaman' =>['required'] ,
            'deadline' => ['required'],
            'book_id' => ['required'],
        ]);

        $book = BorrowBook::create([
            'tgl_peminjaman' =>request('tgl_peminjaman') ,
            'deadline' => request('deadline'),
            'user_id' => auth()->user()->id,
            'book_id' => request('book_id'),
        ]);

        return new BookResource($book);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = BorrowBook::find($id);

        return new BorrowResource($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tgl_pengembalian' =>['required'] ,
            'ontime' => ['required'],
        ]);

        BorrowBook::find($id)->update([
            'tgl_pengembalian' =>request('tgl_pengembalian') ,
            'ontime' => request('ontime'),
        ]);

        $book = BorrowBook::find($id);

        return new BorrowResource($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BorrowBook::find($id)->delete();
    }
}
