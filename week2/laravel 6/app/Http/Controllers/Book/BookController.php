<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Resources\BookResource;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::all();

        return BookResource::collection($book);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'=>['required'],
            'pengarang' => ['required'],
            'tahun' => ['required'],
        ]);

        $book = Book::create([
            'judul'=>request('judul'),
            'pengarang'=>request('pengarang'),
            'tahun'=>request('tahun'),
        ]);
        return $book;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::all()->where('id',$id)->first();

        return new BookResource($book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        request()->validate([
            'judul'=>['required'],
            'pengarang' => ['required'],
            'tahun' => ['required'],
        ]);

        $book = Book::find($id);

        $book->update([
            'judul'=>request('judul'),
            'pengarang'=>request('pengarang'),
            'tahun'=>request('tahun'),
        ]);

        return new BookResource($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Book::find($id)->delete();
        // return 'test';
    }
}
