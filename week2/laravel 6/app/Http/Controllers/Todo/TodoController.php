<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Todo\Todo;
use App\Http\Resources\TodoResource;

class TodoController extends Controller
{
    public function index(){
        $todos = Todo::orderBy('created_at', 'desc')->get();

        return TodoResource::collection($todos);
    }

    public function store(Request $request){
        $request->validate([
            'text'=>['required'],
            'done'=>['required']
        ]);
        
        $todo = Todo::create([
            'text'=> $request->text,
            'done'=> request('done'),
        ]);

        return new TodoResource($todo);
    }

    public function changeDoneStatus($id){
        $todo = Todo::find($id);

        $todo->update([
            'done'=> !$todo->done
        ]);

        return new TodoResource($todo);
    }

    public function destroy($id){
        $todo = $todo_copy = Todo::find($id);

        $todo->delete();
        
        return new TodoResource($todo_copy);
    }
}
