<?php

namespace App\Models\Todo;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $guarded = [];
}
