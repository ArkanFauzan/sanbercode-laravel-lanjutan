Vue.component('comments',{
    template: '#comment-template',
    props: ['comment'],
    data: function(){
        return {
            plus: false,
            minus: false
        }
    },
    methods:{
        tambah: function(){
            this.plus = !this.plus
            this.minus = false
        },
        kurang: function(){
            this.minus = !this.minus
            this.plus = false
        }
    },
    computed:{
        score: function(){
            if(this.plus){
                return this.comment.score + 1
            }
            else if(this.minus){
                return this.comment.score - 1
            }
            else{
                return this.comment.score 
            }
        }
    }
});

var vm = new Vue({
    el:'#app',
    data:{
        comments:[
            {body:'komentar 1 komentar', date:'10-10-2020', score:0},
            {body:'komentar 2 komentar', date:'10-10-2020', score:5},
            {body:'komentar 3 komentar', date:'10-10-2020', score:2}
        ]
    }
});