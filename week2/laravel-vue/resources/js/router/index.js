import Home from '../views/Home'
import About from '../views/About'
import Contact from '../views/Contact'
import CreateNote from '../views/notes/Create'
import ListOfNote from '../views/notes/List'
import ShowTheNote from '../views/notes/Show'
import EditTheNote from '../views/notes/Edit'

export default {
    mode:'history',
    linkActiveClass: 'active',
    routes: [
        {
            path:'/',
            name:'home',
            component: Home
        },
        {
            path:'/about',
            name:'about',
            component: About
        },
        {
            path:'/contact',
            name:'contact',
            component: Contact
        },
        {
            path:'/notes/create',
            name: 'notes.create',
            component: CreateNote
        },
        {
            path:'/notes/list',
            name: 'notes.list',
            component: ListOfNote
        },
        {
            path:'/notes/:slug',
            name: 'notes.show',
            component: ShowTheNote
        },
        
        {
            path:'/notes/:slug/edit',
            name: 'notes.edit',
            component: EditTheNote
        }
    ]
}