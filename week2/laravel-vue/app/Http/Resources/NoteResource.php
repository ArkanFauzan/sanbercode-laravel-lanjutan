<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'subject_id'=>$this->subject_id,
            'subject_name'=>$this->subject->name,
            'title'=>$this->title,
            'slug'=>$this->slug,
            'body'=>$this->body,
            'published'=>$this->created_at->format('d F, Y'),
            'link'=>route('notes.show',['note'=>$this->slug])
        ];
    }
}
