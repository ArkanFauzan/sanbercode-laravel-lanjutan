<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Note,Subject};
use App\Http\Resources\NoteResource;

class NoteController extends Controller
{
    public function index(){
        $notes = Note::latest()->get();
        return NoteResource::collection($notes);
    }

    public function subject(){
        $subjects = Subject::all();
        return response()->json(['data'=>$subjects],200);
    }

    public function store(Request $request){
        sleep(1);
        $request->validate([
            'subject_id'=>['required','exists:subjects,id'],
            'title'=>['required'],
            'body'=>['required']
        ]);

        $note = Note::create([
            'subject_id'=>request('subject_id'),
            'title'=>request('title'),
            'slug'=>\Str::slug(request('title')),
            'body'=>request('body')
        ]);

        return response()->json(["message"=>'Note berhasil ditambahkan','note'=>$note],200);
    }

    public function show(Note $note){
        return new NoteResource($note);
    }

    public function update(Request $request,Note $note){
        sleep(0.5);
        $request->validate([
            'subject_id'=>['required','exists:subjects,id'],
            'title'=>['required'],
            'body'=>['required']
        ]);

        $note->update([
            'subject_id'=>request('subject_id'),
            'title'=>request('title'),
            'body'=>request('body')
        ]);

        return response()->json(["message"=>'Note berhasil diupdate','note'=>$note],200);
    }

    public function destroy(Note $note){
        $note->delete();
        return response()->json(['message'=>'Note berhasil dihapus'],200);
    }
}
