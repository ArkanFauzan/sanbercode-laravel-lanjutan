<?php

use App\Http\Controllers\NoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('notes')->group(function(){
    Route::get('','NoteController@index');
    Route::post('','NoteController@store');
    Route::get('/{note:slug}','NoteController@show')->name('notes.show');
    Route::patch('/{note:slug}','NoteController@update');
    Route::delete('/{note:slug}','NoteController@destroy');
});

Route::get('subject', 'NoteController@subject');
