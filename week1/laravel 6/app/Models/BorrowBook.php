<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BorrowBook extends Model
{
    protected $table = 'borrow_book';
    protected $guarded = [];

    public function book(){
        return $this->belongsTo(Book::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
