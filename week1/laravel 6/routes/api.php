<?php

Route::namespace('Auth')->group(function(){
    Route::post('/register','RegisterController');
    Route::post('/login','LoginController');
    Route::post('/logout','LogoutController');

});

Route::namespace('Book')->middleware("auth:api")->group(function(){
    Route::post('/book','BookController@store');
    Route::delete('/book/{id}','BookController@destroy');
    Route::post('/book/{id}','BookController@update');
    Route::get('/book','BookController@index');
    Route::get('/book/{id}','BookController@show');

    Route::get('/borrow-book','BorrowController@index');
    Route::get('/borrow-book/{id}','BorrowController@show');
    Route::post('/borrow-book','BorrowController@store');
    Route::post('/borrow-book/{id}','BorrowController@update')->middleware('RoleMiddleware');
    Route::delete('/borrow-book/{id}','BorrowController@destroy');
});

Route::get('/user','UserController');