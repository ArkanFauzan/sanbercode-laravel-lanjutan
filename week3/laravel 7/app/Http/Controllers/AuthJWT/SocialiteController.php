<?php

namespace App\Http\Controllers\AuthJWT;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use App\SocialUser;
use JWTAuth;
use Auth;

class SocialiteController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $socialUser = Socialite::driver($provider)->stateless()->user();
        } catch (Exception $th) {
            return response()->json(null,401);
        }
        
        $authUser = $this->findOrCreateUser($socialUser, $provider);

        $token = JWTAuth::fromUser($authUser);

        return response()->json(compact('token'),200);
    }

    public function findOrCreateUser($socialUser, $provider){
        // cek apakah ada data sosmed user
        $socialAccount = SocialUser::where([
                            'provider_id'=>$socialUser->getId(),
                            'provider_name'=> $provider
                        ])
                        ->first();
        
        if($socialAccount){
            // berdasarkan relasi tabel sosmed ke tabel user
            return $socialAccount->user;
        }
        else{
            // cek apakah ada data user dengan email sosmed tersebut
            $user = User::where('email',$socialUser->getEmail())->first();

            if(!$user){
                $user = User::create([
                    'name'=> $socialUser->getName(),
                    'email'=> $socialUser->getEmail(),
                ]);
            }

            // tambahkan data sosmed ke tabel social user
            $user->social_accounts()->create([
                'provider_id'=>$socialUser->getId(),
                'provider_name'=>$provider
            ]);
            
            return $user;

        }
    }
}
