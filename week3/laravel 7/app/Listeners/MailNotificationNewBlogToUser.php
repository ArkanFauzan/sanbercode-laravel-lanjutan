<?php

namespace App\Listeners;

use App\Events\NewBlog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\MailSuccessCreateNewBlog;
use Mail;

class MailNotificationNewBlogToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlog  $event
     * @return void
     */
    public function handle(NewBlog $event)
    {
        Mail::to($event->blog->user)->send(new MailSuccessCreateNewBlog($event->blog));
    }
}
