<?php

namespace App\Listeners;

use App\Events\NewBlog;
use App\Mail\MailSuccessCreateNewBlog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\MailNewBlogNotification;
use Mail;
use App\User;

class MailNotificationNewBlogToAdmin implements ShouldQueue
{
    public $admin = '';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->admin = User::where(['role'=>'admin'])->get();
    }

    /**
     * Handle the event.
     *
     * @param  NewBlog  $event
     * @return void
     */
    public function handle(NewBlog $event)
    {
        Mail::to($this->admin)->send(new MailNewBlogNotification($event->blog));
    }
}
