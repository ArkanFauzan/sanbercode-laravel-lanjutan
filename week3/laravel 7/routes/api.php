<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Blog')->middleware('auth:api')->group(function(){
    Route::post('/blog','BlogController@store');
});

Route::namespace('AuthJWT')->group(function(){
    Route::post('/register','RegisterController');
    Route::post('/login','LoginController');
    Route::post('/logout','LogoutController');

    Route::get('/login/{provider}','SocialiteController@redirectToProvider');
    Route::get('/login/{provider}/callback','SocialiteController@handleProviderCallback');
});